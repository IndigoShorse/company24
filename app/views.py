from django.shortcuts import render
from app.models import Article, HeaderName


# Create your views here.
def index(request):
    articles = Article.objects.all()
    return render(request, 'index.html', {'articles': articles})


def gallery(request):
    articles = Article.objects.all()
    return render(request, 'gallery.html', {'articles': articles})
