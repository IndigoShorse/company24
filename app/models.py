from django.db import models


class HeaderName(models.Model):
    name = models.CharField(max_length=100, verbose_name="Заголовок")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Имя заголовка"
        verbose_name_plural = "Имя заголовка"


class Article(models.Model):
    image = models.ImageField(upload_to='pics', verbose_name='Фотография')
    article = models.CharField(default="Text", max_length=40, verbose_name="Заголовок")
    text = models.TextField(verbose_name="Текст новости")
    
    def __str__(self):
        return self.article

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"

class Photo(models.Model):
    image = models.ImageField(upload_to='gallery', verbose_name='Картинка')
    text = models.CharField(default="Text", max_length=40, verbose_name="Подпись")
