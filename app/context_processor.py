from app.models import HeaderName


def header_name(request):
    try:
        name = HeaderName.objects.get(pk=1).name
        return {'header_name': name}
    except HeaderName.DoesNotExist:
        return {'header_name': 'company24'}
