const urls = {
    "/" : document.getElementById("index"),
    "/about" : document.getElementById("about"),
    "/career" : document.getElementById("career"),
    "/gallery" : document.getElementById("gallery"),
    "/services" : document.getElementById("services"),
    "/testimonials" : document.getElementById("testimonials"),
}
function light_nav(){
    let url = window.location.pathname
    console.log(url)
    urls[url].classList.add("active")
}
window.onload = light_nav